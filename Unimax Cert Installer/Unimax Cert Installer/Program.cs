﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Unimax_Cert_Installer
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = string.Empty;
            //if (IntPtr.Size == 8) path = Directory.GetCurrentDirectory() + @"\x64\certmgr.exe";
            //else
            path = Directory.GetCurrentDirectory() + @"\x86\certmgr.exe";
            ProcessStartInfo psi = new ProcessStartInfo(path);
            psi.Arguments = "-add " + Directory.GetCurrentDirectory() + "UnimaxCA.cer -s -r localMachine root";
            psi.UseShellExecute = false;
            psi.CreateNoWindow = true;
            Process.Start(psi);
            psi.Arguments = "-add " + Directory.GetCurrentDirectory() + "UnimaxCA.cer -s -r currentUser root";
            Process.Start(psi);
        }
    }
}
